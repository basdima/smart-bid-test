export enum Color {
  Red = 'red',
  Green = 'green',
  Blue = 'blue',
  Java = 'java',
  Orange = 'orange'
}
