import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IMessageItem } from '../../../models/messages.model';

@Component({
  selector: 'app-widget-message-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  templateUrl: './message-item.component.html',
  styleUrls: [ './message-item.component.scss' ]
})
export class MessageItemComponent {
  @Input() public message: IMessageItem;
}
