import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IMessagesModel } from '../../models/messages.model';

@Component({
  selector: 'app-widget-messages',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  templateUrl: './messages-widget.component.html',
  styleUrls: [ './messages-widget.component.scss' ]
})
export class MessagesWidgetComponent {
  @Input() public model: IMessagesModel;
}
