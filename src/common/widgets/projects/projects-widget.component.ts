import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IProjectsModel } from '../../models/projects.model';

@Component({
  selector: 'app-widget-project-list',
  templateUrl: './projects-widget.component.html',
  styleUrls: [ './projects-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class ProjectsWidgetComponent {
  @Input() public model: IProjectsModel;
}
