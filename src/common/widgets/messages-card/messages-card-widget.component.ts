import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IMessagesModel } from '../../models/messages.model';

@Component({
  selector: 'app-widget-messages-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  templateUrl: './messages-card-widget.component.html',
  styleUrls: [ './messages-card-widget.component.scss' ]
})
export class MessagesCardWidgetComponent {
  @Input() public model: IMessagesModel;
}
