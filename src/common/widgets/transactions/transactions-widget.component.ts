import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { TransactionsModel } from '../../models/transactions.model';

@Component({
  selector: 'app-widget-transactions',
  templateUrl: './transactions-widget.component.html',
  styleUrls: [ './transactions-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class TransactionsWidgetComponent {
  @Input() public model: TransactionsModel;
}
