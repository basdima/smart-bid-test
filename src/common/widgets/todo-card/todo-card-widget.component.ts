import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { TodoModel } from '../../models/todo.model';

@Component({
  selector: 'app-widget-todo-card',
  templateUrl: './todo-card-widget.component.html',
  styleUrls: [ './todo-card-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class TodoCardWidgetComponent {
  @Input() model: TodoModel;
}
