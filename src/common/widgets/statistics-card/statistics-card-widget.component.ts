import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IStatisticsModel } from '../../models/statistics.model';

@Component({
  selector: 'app-widget-statistics-card',
  templateUrl: './statistics-card-widget.component.html',
  styleUrls: [ './statistics-card-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class StatisticsCardWidgetComponent {
  @Input() model: IStatisticsModel;
}
