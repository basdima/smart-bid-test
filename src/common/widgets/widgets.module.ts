import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChartWidgetComponent } from './chart/chart-widget.component';
import { ElementsModule } from '../elements/elements.module';
import { StatisticsCardWidgetComponent } from './statistics-card/statistics-card-widget.component';
import { StatisticsWidgetComponent } from './statistics/statistics-widget.component';
import { MessagesCardWidgetComponent } from './messages-card/messages-card-widget.component';
import { MessagesWidgetComponent } from './messages/messages-widget.component';
import { MessageItemComponent } from './messages/message-item/message-item.component';
import { ProjectsWidgetComponent } from './projects/projects-widget.component';
import { ProjectsCardWidgetComponent } from './projects-card/projects-card-widget.component';
import { TodoCardWidgetComponent } from './todo-card/todo-card-widget.component';
import { TodoWidgetComponent } from './todo/todo-widget.component';
import { TransactionsCardWidgetComponent } from './transactions-card/transactions-card-widget.component';
import { TransactionsWidgetComponent } from './transactions/transactions-widget.component';

const localComponents = [
  MessageItemComponent
];
const exportList = [
  ChartWidgetComponent,
  StatisticsWidgetComponent,
  StatisticsCardWidgetComponent,
  MessagesCardWidgetComponent,
  MessagesWidgetComponent,
  ProjectsWidgetComponent,
  ProjectsCardWidgetComponent,
  TodoCardWidgetComponent,
  TodoWidgetComponent,
  TransactionsCardWidgetComponent,
  TransactionsWidgetComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ElementsModule
  ],
  declarations: [
    ... exportList,
    ... localComponents
  ],
  exports: [
    ... exportList
  ]
})
export class WidgetsModule {
}
