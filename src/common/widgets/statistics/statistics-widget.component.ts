import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IStatisticsModel } from '../../models/statistics.model';

@Component({
  selector: 'app-widget-statistics',
  templateUrl: './statistics-widget.component.html',
  styleUrls: [ './statistics-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})

export class StatisticsWidgetComponent {
  @Input() model: IStatisticsModel;
  @Input() showProgressBar = false;
  @Input() rateDefaultTextColor = false;

  public get rateValue(): number {
    return (this.model.value / this.model.maxValue) * 100;
  }
}
