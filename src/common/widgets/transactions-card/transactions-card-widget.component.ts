import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { TransactionsModel } from '../../models/transactions.model';

@Component({
  selector: 'app-widget-transactions-card',
  templateUrl: './transactions-card-widget.component.html',
  styleUrls: [ './transactions-card-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class TransactionsCardWidgetComponent {
  @Input() model: TransactionsModel;
}
