import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IProjectsModel } from '../../models/projects.model';

@Component({
  selector: 'app-widget-projects-card',
  templateUrl: './projects-card-widget.component.html',
  styleUrls: [ './projects-card-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class ProjectsCardWidgetComponent {
  @Input() public model: IProjectsModel;
}
