import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { TodoModel } from '../../models/todo.model';

@Component({
  selector: 'app-widget-todo',
  templateUrl: './todo-widget.component.html',
  styleUrls: [ './todo-widget.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class TodoWidgetComponent {
  @Input() public model: TodoModel;
}
