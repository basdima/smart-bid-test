import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { IChartWidgetData } from './chart-widget.model';

@Component({
  selector: 'app-widget-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  templateUrl: './chart-widget.component.html',
  styleUrls: [ './chart-widget.component.scss' ]
})
export class ChartWidgetComponent {
  @Input() public data: IChartWidgetData;
}
