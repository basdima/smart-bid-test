import { Color } from '../../consts/colors.const';

export interface TChartStatisticsItem {
  description: string;
  value: number;
  maxValue: number;
  icon: string;
  color: Color;
}
export interface IChartWidgetData {
  statistics: Array<TChartStatisticsItem>;
  // TODO: Temporarily until the exact structure is determined
  [ K: string ]: any;
}
