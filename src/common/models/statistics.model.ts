import { RateDirection } from '../elements/rate/rate.component';
import { Color } from '../consts/colors.const';

export interface IStatisticsModel {
  name?: string;
  type?: string;
  color?: Color;
  value: number;
  maxValue: number;
  direction: RateDirection;
  description: string;
}
