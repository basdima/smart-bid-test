export interface IMessageItem {
  from: string;
  subject: string;
  ago: string;
  datetime: string;
  last: boolean;
}

export interface IMessagesModel {
  newMessagesCount: number;
  draftMessagesCount: number;
  messages: Array<IMessageItem>;
}
