import { Status } from '../elements/status/status.component';
import { RateDirection } from '../elements/rate/rate.component';



export interface IProjectItem {
  status: Status;
  date: string;
  user: string;
  value: number;
  direction: RateDirection;
}

export type IProjectsModel = Array<IProjectItem>;

