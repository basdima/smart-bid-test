export interface ITransactionItem {
  id: number;
  name: string;
  date: string;
  amount: number;
  lowLevel: boolean;
}

export type TransactionsModel = Array<ITransactionItem>;
