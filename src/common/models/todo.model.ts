export interface ITodoItem {
  text: string;
  completed: boolean;
  duration?: string;
}
export type TodoModel = Array<ITodoItem>;
