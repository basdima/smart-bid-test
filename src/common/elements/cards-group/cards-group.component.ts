import { ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'ui-cards-group',
  templateUrl: './cards-group.component.html',
  styleUrls: ['./cards-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class CardsGroupComponent {
  @Input() columns = 1;
  @HostBinding('class') get hostClass() {
    return 'cols-' + this.columns;
  }
}
