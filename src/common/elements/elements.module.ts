import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { CardsGroupComponent } from './cards-group/cards-group.component';
import { IconComponent } from './icon/icon.component';
import { CardHeaderActionsDirective } from './card/card-header-actions.directive';
import { ToggleComponent } from './toggle/toggle.component';
import { CardHeaderActionsBarComponent } from './card-actions/card-header-actions-bar.component';
import { LabelComponent } from './label/label.component';
import { StatusComponent } from './status/status.component';
import { RateComponent } from './rate/rate.component';
import { GridComponent } from './grid/grid.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { FormsModule } from '@angular/forms';

const exportList = [
  CardComponent,
  CardsGroupComponent,
  CardHeaderActionsDirective,
  ToggleComponent,
  IconComponent,
  LabelComponent,
  CardHeaderActionsBarComponent,
  StatusComponent,
  RateComponent,
  GridComponent,
  CheckboxComponent
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    ... exportList
  ],
  exports: [
    ... exportList
  ]
})
export class ElementsModule {
}
