import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef, ViewEncapsulation } from '@angular/core';
import { CardHeaderActionsDirective } from './card-header-actions.directive';

@Component({
  selector: 'ui-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})
export class CardComponent {
  @Input() title: string;

  @ContentChild(CardHeaderActionsDirective, {read: TemplateRef}) actions;

}
