import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[ui-grid]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  template: `<ng-content></ng-content>`,
  styleUrls: [ './grid.component.scss' ]
})
export class GridComponent {
}

