import { Component, HostBinding, Input } from '@angular/core';
import { Color } from '../../consts/colors.const';

@Component({
  selector: 'ui-label',
  templateUrl: './label.component.html',
  styleUrls: [
    './label.component.scss'
  ]
})
export class LabelComponent {
  @Input() color: Color;

  @HostBinding('class') get ClassList(): string {
    const classList = [];
    if (this.color) {
      classList.push(this.color + '-bg');
    }
    return classList.join(' ');
  }
}
