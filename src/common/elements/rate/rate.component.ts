import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { Color } from '../../consts/colors.const';

export enum RateDirection {
  NONE_PASSIVE,
  NONE_ACTIVE,
  UP,
  UP_LOW,
  DOWN,
  DOWN_LOW
}

export enum RateIconPosition {
  LEFT = 'left',
  RIGHT = 'right'
}

@Component({
  selector: 'ui-rate',
  template: `
    <ui-icon *ngIf="isLeftIcon" [icon]="icon" class="color-{{color}} left"></ui-icon>
    <span class="color-{{ defaultTextColor ? 'pale-sky' : color}}">{{ valueText }}</span>
    <ui-icon *ngIf="isRightIcon" [icon]="icon" class="color-{{color}} right"></ui-icon>
  `,
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './rate.component.scss'
  ]
})
export class RateComponent {
  @Input() value: number;
  @Input() direction: RateDirection;
  @Input() iconPosition: RateIconPosition = RateIconPosition.RIGHT;
  @Input() ratePrefix = '';
  @Input() rateSuffix = '%';
  @Input() defaultTextColor = false;

  public get valueText(): string {
    return this.ratePrefix + Math.trunc(this.value) + this.rateSuffix;
  }

  public get isLeftIcon(): boolean {
    return this.iconPosition === RateIconPosition.LEFT;
  }

  public get isRightIcon(): boolean {
    return this.iconPosition === RateIconPosition.RIGHT;
  }

  public get icon(): string {
    switch (this.direction) {
      case RateDirection.DOWN:
      case RateDirection.DOWN_LOW: {
        return 'level-down';
      }
      case RateDirection.UP:
      case RateDirection.UP_LOW:  {
        return 'level-up';
      }
      case RateDirection.NONE_PASSIVE:
      case RateDirection.NONE_ACTIVE: {
        return 'bolt';
      }
    }
  }

  public get color(): string {
    switch (this.direction) {
      case RateDirection.DOWN_LOW: {
        return Color.Orange;
      }
      case RateDirection.DOWN: {
        return Color.Red;
      }
      case RateDirection.UP_LOW: {
        return Color.Java;
      }
      case RateDirection.NONE_ACTIVE:
      case RateDirection.UP: {
        return Color.Green;
      }
      case RateDirection.NONE_PASSIVE: {
        return Color.Blue;
      }
    }
  }
}
