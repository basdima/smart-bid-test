import { Component, Input } from '@angular/core';
import { Color } from '../../consts/colors.const';

export enum Status {
  PENDING,
  COMPLETED,
  CANCELED
}

@Component({
  selector: 'ui-status',
  template: `
    <ui-label [color]="color">{{ text }}</ui-label>
  `
})
export class StatusComponent {
  @Input() status: Status;

  public get color(): Color {
    switch (this.status) {
      case Status.COMPLETED: { return Color.Green; }
      case Status.CANCELED: { return Color.Orange; }
      default: { return null; }
    }
  }

  public get text(): string {
    switch (this.status) {
      case Status.COMPLETED: { return 'Completed'; }
      case Status.CANCELED: { return 'Canceled'; }
      case Status.PENDING: { return 'Pending...'; }
      default: { return ''; }
    }
  }
}
