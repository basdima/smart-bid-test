import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-card-header-actions-bar',
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [ './card-header-actions-bar.component.scss' ],
  templateUrl: './card-header-actions-bar.component.html'
})
export class CardHeaderActionsBarComponent {
}
