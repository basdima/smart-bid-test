import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: [ './toggle.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated
})

export class ToggleComponent {
  @Input() buttons: {[K: string]: string};
  @Input() active: string;

  @Output() change = new EventEmitter<string>();

  public get buttonsCollection() {
    return Object.keys(this.buttons).map(key => {
      return { key: key, text: this.buttons[key] };
    });
  }

  public buttonClickHandler($event, button) {
    this.active = button.key;
    this.change.emit(this.active);
  }
}
