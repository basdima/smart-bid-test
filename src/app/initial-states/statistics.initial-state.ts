import { IStatisticsModel } from '../../common/models/statistics.model';
import { RateDirection } from '../../common/elements/rate/rate.component';
import { Color } from '../../common/consts/colors.const';

export const statistics: IStatisticsModel[] = [
  {
    name: 'Income',
    type: 'Monthly',
    value: 40886.2,
    maxValue: 41720.61,
    description: 'Total income',
    direction: RateDirection.NONE_PASSIVE,
    color: Color.Blue
  },
  {
    name: 'Orders',
    type: 'Annual',
    value: 275.8,
    maxValue: 1379,
    description: 'New orders',
    direction: RateDirection.UP_LOW,
    color: Color.Java
  },
  {
    name: 'Visits',
    type: 'Today',
    value: 106.12,
    maxValue: 241.18,
    description: 'New visits',
    direction: RateDirection.UP,
    color: Color.Green
  },
  {
    name: 'User activity',
    type: 'Low value',
    value: 80.6,
    maxValue: 212.1,
    description: 'In first month',
    direction: RateDirection.DOWN,
    color: Color.Red
  }
];
