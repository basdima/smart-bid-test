import { TransactionsModel } from '../../common/models/transactions.model';

export const transactions: TransactionsModel = [
  {
    id: 1,
    name: 'Security doors',
    date: '16 jun 2014',
    amount: 483,
    lowLevel: false
  },
  {
    id: 2,
    name: 'Wardrobes',
    date: '10 jun 2014',
    amount: 327,
    lowLevel: false
  },
  {
    id: 3,
    name: 'Set of tools',
    date: '12 jun 2014',
    amount: 125,
    lowLevel: true
  },
  {
    id: 4,
    name: 'Panoramic pictures',
    date: '22 jun 2013',
    amount: 344,
    lowLevel: false
  },
  {
    id: 5,
    name: 'Phones',
    date: '24 jun 2013',
    amount: 235,
    lowLevel: false
  },
  {
    id: 6,
    name: 'Monitors',
    date: '26 jun 2013',
    amount: 100,
    lowLevel: false
  },
];
