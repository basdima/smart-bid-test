import { IMessagesModel } from '../../common/models/messages.model';

export const messages: IMessagesModel = {
  newMessagesCount: 22,
  draftMessagesCount: 16,
  messages: [
    {
      from: 'Monica Smith',
      subject: 'Lorem Ipsum is simply dummy text of the printing and typesetting Industry. Lorem Ipsum',
      ago: '1m ago',
      datetime: 'Today 5:60 pm - 12.06.2014',
      last: true
    },
    {
      from: 'Jogn Angel',
      subject: 'There are many variations of passages of Lorem Ipsum available',
      ago: '2m ago',
      datetime: 'Today 2:23 pm - 11.06.2014',
      last: false
    },
    {
      from: 'Jesica Ocean',
      subject: 'Contrary to popular belief, Lorem Ipsum',
      ago: '5m ago',
      datetime: 'Today 1:00 pm - 10.06.2014',
      last: false
    },
    {
      from: 'Monica Jackson',
      subject: 'The generated Lorem Ipsum is therefore',
      ago: '5m ago',
      datetime: 'Yesterday 8:48 pm - 10.06.2014',
      last: false
    },
    {
      from: 'Anna Legend',
      subject: 'All the Lorem Ipsum generators on the Internet tend to repeat',
      ago: '5m ago',
      datetime: 'Yesteday 8:48 pm - 10.06.2014',
      last: false
    },
    {
      from: 'Damian Nowak',
      subject: 'The standart chunk of Lorem Ipsum used',
      ago: '5m ago',
      datetime: 'Yesterday 8:48 px - 10.06.2014',
      last: false
    },
    {
      from: 'Gary Smith',
      subject: '200 Latin words, combined with a handful',
      ago: '',
      datetime: 'Yesterday 8:48 px - 10.06.2014',
      last: false
    }
  ]
};
