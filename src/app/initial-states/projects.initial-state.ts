import { IProjectsModel } from '../../common/models/projects.model';
import { Status } from '../../common/elements/status/status.component';
import { RateDirection } from '../../common/elements/rate/rate.component';

export const projects: IProjectsModel = [
  {
    status: Status.PENDING,
    date: '11:20pm',
    user: 'Samantha',
    value: 24,
    direction: RateDirection.UP
  },
  {
    status: Status.CANCELED,
    date: '11:40am',
    user: 'Monica',
    value: 66,
    direction: RateDirection.UP
  },
  {
    status: Status.PENDING,
    date: '01:30pm',
    user: 'John',
    value: 54,
    direction: RateDirection.UP
  },
  {
    status: Status.PENDING,
    date: '02:20pm',
    user: 'Agnes',
    value: 12,
    direction: RateDirection.UP
  },
  {
    status: Status.COMPLETED,
    date: '04:10am',
    user: 'Amelia',
    value: 66,
    direction: RateDirection.UP
  },
  {
    status: Status.PENDING,
    date: '12:08am',
    user: 'Damian',
    value: 23,
    direction: RateDirection.UP
  }
];
