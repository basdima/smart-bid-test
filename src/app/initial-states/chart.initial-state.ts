import { RateDirection } from '../../common/elements/rate/rate.component';

export const chartData = {
  data: [],
  statistics: [
    {
      description: 'Total orders in period',
      value: 2.346,
      maxValue: 8.378,
      direction: RateDirection.UP
    },
    {
      description: 'Orders in last month',
      value: 4.422,
      maxValue: 7.370,
      direction: RateDirection.DOWN_LOW
    },
    {
      description: 'Monthly income from orders',
      value: 9.180,
      maxValue: 41.727,
      direction: RateDirection.NONE_ACTIVE
    }
  ]
};
