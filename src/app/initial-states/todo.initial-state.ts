import { TodoModel } from '../../common/models/todo.model';

export const todoList: TodoModel = [
  {
    text: 'Buy a milk',
    completed: true
  },
  {
    text: 'Go to shop and find some products.',
    completed: false
  },
  {
    text: 'Send document to Mike',
    completed: false,
    duration: '1 min'
  },
  {
    text: 'Go to the Doctor Smith',
    completed: true
  },
  {
    text: 'Plan vocation',
    completed: false
  },
  {
    text: 'Create new stuff',
    completed: false
  },
  {
    text: 'Call to Anna for dinner',
    completed: false
  }
];
