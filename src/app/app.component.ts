import { Component, ViewEncapsulation } from '@angular/core';

import { statistics } from './initial-states/statistics.initial-state';
import { chartData } from './initial-states/chart.initial-state';
import { messages } from './initial-states/messages.initial-state';
import { projects } from './initial-states/projects.initial-state';
import { todoList } from './initial-states/todo.initial-state';
import { transactions } from './initial-states/transactions.initial-state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  encapsulation: ViewEncapsulation.Emulated
})
export class AppComponent {
  public statistics = statistics;
  public chartData = chartData;
  public messages = messages;
  public projects = projects;
  public todoList = todoList;
  public transactions = transactions;
}
